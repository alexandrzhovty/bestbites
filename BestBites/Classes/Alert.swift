//
//  Alert.swift
//  BestBites
//
//  Created by Aleksandr Zhovtyi on 5/10/18.
//Copyright © 2018 Aleksandr Zhovtyi. All rights reserved.
//

import UIKit


final class Alert {
    static let `default` = Alert()
    
    var popupWindow : UIWindow!
    var rootVC : UIViewController!
    
    
    @available(*, deprecated, message: "Use `default` property instead")
    class var sharedInstance: Alert {
        struct SingletonWrapper {
            static let sharedInstance = Alert()
        }
        
        return SingletonWrapper.sharedInstance;
    }
    
    
    fileprivate init() {
        let screenBounds = UIScreen.main.bounds
        popupWindow = UIWindow(frame: CGRect(x: 0, y: 0, width: screenBounds.width, height: screenBounds.height))
        popupWindow.windowLevel = UIWindow.Level.statusBar + 1
        
        rootVC = StatusBarShowingViewController()
        popupWindow.rootViewController = rootVC
        
    }
    
    func showButton(_ title: String, message: String, buttonTitle: String, onComplete: @escaping ()->Void = {  }) {
        DispatchQueue.main.async {
            self.popupWindow.isHidden = false
            let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: buttonTitle, style: UIAlertAction.Style.default, handler: { _ in
                self.resignPopupWindow()
                onComplete()
            }))
            self.rootVC.present(alert, animated: true, completion: nil)
        }
    }
    
    
    func showOk(_ title: String, message: String, onComplete: @escaping ()->Void = {  }) {
        DispatchQueue.main.async {
            self.popupWindow.isHidden = false
            let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { _ in
                self.resignPopupWindow()
                onComplete()
            }))
            self.rootVC.present(alert, animated: true, completion: nil)
        }
    }
    
    
    func showError(message: String, onComplete: @escaping ()->Void = {  }) {
        DispatchQueue.main.async {
            self.popupWindow.isHidden = false
            
            
            
            let title = NSLocalizedString("Error", comment: "")
            let OKtitle = NSLocalizedString("OK", comment: "")
            
            let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: OKtitle, style: UIAlertAction.Style.default, handler: { _ in
                self.resignPopupWindow()
                onComplete()
            }))
            self.rootVC.present(alert, animated: true, completion: nil)
        }
    }
    
    func showOkCancel(_ title: String, message: String, onComplete: (()->Void)?, onCancel: (()->Void)?) {
        DispatchQueue.main.async {
            self.popupWindow.isHidden = false
            let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { _ in
                self.resignPopupWindow()
                onComplete?()
            })
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: { _ in
                self.resignPopupWindow()
                onCancel?()
            })
            alert.addAction(cancelAction)
            alert.addAction(okAction)
            self.rootVC.present(alert, animated: true, completion: nil)
        }
    }
    
    func showYesNo(_ title: String, message: String, onYes: @escaping ()->Void = {}, onNo: @escaping ()->Void = {}) {
        DispatchQueue.main.async {
            self.popupWindow.isHidden = false
            let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
            let okAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { _ in
                self.resignPopupWindow()
                onYes()
            })
            let cancelAction = UIAlertAction(title: "No", style: UIAlertAction.Style.default, handler: { _ in
                self.resignPopupWindow()
                onNo()
            })
            alert.addAction(cancelAction)
            alert.addAction(okAction)
            
            self.rootVC.present(alert, animated: true, completion: nil)
        }
    }
    
    
    
    
    func showOkNo(_ title: String, message: String, onOk: @escaping ()->Void = {}, onNo: @escaping ()->Void = {}) {
        DispatchQueue.main.async {
            self.popupWindow.isHidden = false
            let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
            let letsGoAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { _ in
                self.resignPopupWindow()
                onOk()
            })
            let laterAction = UIAlertAction(title: "No", style: UIAlertAction.Style.default, handler: { _ in
                self.resignPopupWindow()
                onNo()
            })
            alert.addAction(laterAction)
            alert.addAction(letsGoAction)
            
            self.rootVC.present(alert, animated: true, completion: nil)
        }
    }
    
    func resignPopupWindow() {
        DispatchQueue.main.async {
            self.popupWindow.isHidden = true
        }
    }
    
}

final class StatusBarShowingViewController: UIViewController {
    override var prefersStatusBarHidden : Bool {
        return true
    }
    
}
