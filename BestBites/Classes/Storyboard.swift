//
//  Storyboard.swift
//  BestBites
//
//  Created by Aleksandr Zhovtyi on 5/3/18.
//Copyright © 2018 Aleksandr Zhovtyi. All rights reserved.
//

import UIKit

public enum StroyboadType: String, Iteratable {
    case main
    case onboarding
    
    var filename: String { return rawValue.capitalized }
}

typealias Storyboard = UIStoryboard

extension Storyboard {
    convenience init(_ storyboard: StroyboadType) {
        self.init(name: storyboard.filename, bundle: nil)
    }
    
    /// Instantiates and returns the view controller with the specified identifier.
    ///
    /// - Parameter identifier: uniquely identifies equals to Class name
    /// - Returns: The view controller corresponding to the specified identifier string. If no view controller is associated with the string, this method throws an exception.
    public func instantiateViewController<T>(_ identifier: T.Type) -> T where T: UIViewController {
        let className = String(describing: identifier)
        guard let vc =  self.instantiateViewController(withIdentifier: className) as? T else {
            fatalError("Cannot find controller with identifier \(className)")
        }
        return vc
    }
}
