//
//  View.swift
//  BestBites
//
//  Created by Aleksandr Zhovtyi on 8/20/18.
//Copyright © 2018 Aleksandr Zhovtyi. All rights reserved.
//

import UIKit

extension UIView {
    
    /// Returns the UIViewController object that manages the receiver.
    ///
    /// - Returns: UIViewController
    public func viewController() -> UIViewController? {
        
        var nextResponder: UIResponder? = self
        
        repeat {
            nextResponder = nextResponder?.next
            
            if let viewController = nextResponder as? UIViewController {
                return viewController
            }
            
        } while nextResponder != nil
        
        return nil
    }
    
    public func addConstraint(to view: UIView, margin: CGFloat = 0) {
        
        view.translatesAutoresizingMaskIntoConstraints = false
        
        view.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: margin).isActive = true
        view.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: margin).isActive = true
        view.topAnchor.constraint(equalTo: self.topAnchor, constant: margin).isActive = true
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: margin).isActive = true
    }
    
    
    func captureScreen() -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, UIScreen.main.scale)
        self.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
}

// MARK: - Hieracly
extension UIView {
    func add(_ subviews: UIView...) {
        subviews.forEach(addSubview)
    }
}

// MARK: - Animation
extension UIView {
    func animate(_ animation: @autoclosure @escaping () -> Void, duration: TimeInterval = 0.25) {
        UIView.animate(withDuration: duration, animations: animation)
    }
}

// MARK: - Shadow
extension UIView {
    func addShadow(color: UIColor = .black, shadowRadius: CGFloat = 5, shadowOpacity: CFloat = 0.25) {
        self.layer.shadowColor = color.cgColor
        self.layer.shadowRadius = shadowRadius
        self.layer.shadowOffset = CGSize(width: 2.5, height: 2.5)
        self.layer.shadowOpacity = shadowOpacity
        clipsToBounds = false
    }
}

