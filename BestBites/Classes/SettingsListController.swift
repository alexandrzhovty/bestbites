//
//  SettingsListController.swift
//  BestBites
//
//  Created by Aleksandr Zhovtyi on 5/7/18.
//  Copyright © 2018 Aleksandr Zhovtyi. All rights reserved.
//

import UIKit
import CoreLocation
import UserNotifications

final class SettingsListController: UITableViewController {
    @IBOutlet weak var locationDetectionSwitch: UISwitch!
    @IBOutlet weak var allowNotificationsSwitch: UISwitch!
    
    @IBOutlet weak var holderView: UIView!
    deinit {
        unregisterObserver()
    }
    
    private struct Row {
        private init(){}
        static let allowLocationDetection: IndexPath = IndexPath(row: 0, section: 0)
        static let allowNotification: IndexPath = IndexPath(row: 1, section: 0)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        locationDetectionSwitch.isOn = UserDefaults.standard.isTrackingEnabled
        registerObserver()
        
        holderView.layer.cornerRadius = 5
        holderView.clipsToBounds = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configure(locationDetectionSwitch)
        configure(allowNotificationsSwitch)
    }

    
    
    
    private func configure(_ viewToConfig: UIView) {
        switch viewToConfig {
        case locationDetectionSwitch:
//            let status = CLLocationManager.authorizationStatus()
//            locationDetectionSwitch.isOn = status == .authorizedAlways && UserDefaults.standard.isTrackingEnabled
            locationDetectionSwitch.isOn =  UserDefaults.standard.isTrackingEnabled
        case allowNotificationsSwitch:
            let current = UNUserNotificationCenter.current()
            current.getNotificationSettings(completionHandler: { [weak self] (settings) in
                DispatchQueue.main.async {
                    self?.allowNotificationsSwitch.isOn = settings.authorizationStatus == .authorized
                    
                }
            })
            
        default: break
        }
    }
    
    

    @IBAction func valueChangedAllowLocation(switchControl: UISwitch) {
        switch switchControl {
        case locationDetectionSwitch:
            if switchControl.isOn {
                let status = CLLocationManager.authorizationStatus()
                if status != .authorizedWhenInUse  && status != .authorizedAlways {
                    
                    guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                        return
                    }
                    
                    if UIApplication.shared.canOpenURL(settingsUrl) {
                        UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                            print("Settings opened: \(success)") // Prints true
                        })
                    }
                    
                    return
                }
            }
            
            
            let defaults = UserDefaults.standard
            defaults.isTrackingEnabled = switchControl.isOn
            
        case allowNotificationsSwitch:
            let settingsUrl = URL(string: UIApplication.openSettingsURLString)!
            UIApplication.shared.open(settingsUrl)
            
        default: break
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath {
        case Row.allowLocationDetection:
            locationDetectionSwitch.setOn(!locationDetectionSwitch.isOn, animated: true)
            valueChangedAllowLocation(switchControl: locationDetectionSwitch)
            tableView.deselectRow(at: indexPath, animated: true)
            
        case Row.allowNotification:
            locationDetectionSwitch.setOn(!allowNotificationsSwitch.isOn, animated: true)
            valueChangedAllowLocation(switchControl: allowNotificationsSwitch)
            tableView.deselectRow(at: indexPath, animated: true)
            
        default:
            return
        }
    }

}

extension SettingsListController: ObserverProtocol {
    func registerObserver() {
        let center = NotificationCenter.default
        let main = OperationQueue.main
        
        center.addObserver(forName: UIApplication.didBecomeActiveNotification, object: nil, queue: main) {
            [weak self] (note) in
            guard let `self` = self else { return }
            self.configure(self.locationDetectionSwitch)
            self.configure(self.allowNotificationsSwitch)
        }
    }
}
