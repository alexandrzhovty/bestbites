//
//  JSON.swift
//  BestBites
//
//  Created by Aleksandr Zhovtyi on 5/3/18.
//  Copyright © 2018 Aleksandr Zhovtyi. All rights reserved.
//

import Foundation

typealias JSON = [String: Any?]
