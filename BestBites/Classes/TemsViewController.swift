//
//  TemsViewController.swift
//  BestBites
//
//  Created by Aleksandr Zhovtyi on 5/17/18.
//  Copyright © 2018 Aleksandr Zhovtyi. All rights reserved.
//

import UIKit
import WebKit

class TemsViewController: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let url = Bundle.main.url(forResource: "Terms", withExtension: "html")!
        let request = URLRequest(url: url)
        webView.load(request)

        // Do any additional setup after loading the view.
    }

    

}
