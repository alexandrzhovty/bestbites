//
//  Color.swift
//  BestBites
//
//  Created by Aleksandr Zhovtyi on 5/2/18.
//Copyright © 2018 Aleksandr Zhovtyi. All rights reserved.
//

import UIKit

typealias Color = UIColor

//    MARK: - Color types
enum ColorType: UInt32 {
    case pink = 0xff3366
    case crimson = 0xb8083e
    case burgundy = 0x56071f
    case blue = 0x0081f4
    
    var color: UIColor { return UIColor(self) }
}

//    MARK: - Initialization
extension UIColor {
    convenience init(_ type: ColorType) {
        self.init(hexValue: type.rawValue)
    }
    
    convenience init(rgbaValue: UInt32) {
        let red   = CGFloat((rgbaValue >> 24) & 0xff) / 255.0
        let green = CGFloat((rgbaValue >> 16) & 0xff) / 255.0
        let blue  = CGFloat((rgbaValue >>  8) & 0xff) / 255.0
        let alpha = CGFloat((rgbaValue      ) & 0xff) / 255.0
        
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
    
    convenience init(hexValue: UInt32, alpha: CGFloat = 1) {
        let red   = CGFloat((hexValue >> 16) & 0xff) / 255.0
        let green = CGFloat((hexValue >>  8) & 0xff) / 255.0
        let blue  = CGFloat((hexValue      ) & 0xff) / 255.0
        
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
}


//    MARK - Color scheme
extension UIColor {
    
    static var tint: UIColor { return Color(hexValue: 0x253d85) }
    
    enum Background {
        static var navigation: UIColor { return UIColor(.pink) }
    }
}
