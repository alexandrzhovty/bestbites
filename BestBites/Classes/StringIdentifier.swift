//
//  StringIdentifier.swift
//  BestBites
//
//  Created by Aleksandr Zhovtyi on 5/4/18.
//  Copyright © 2018 Aleksandr Zhovtyi. All rights reserved.
//


import Foundation

struct StringIdentifier: RawRepresentable, ExpressibleByStringLiteral  {
    let rawValue: String
    
    init(rawValue: String) {
        self.rawValue = rawValue
    }
    
    init(stringLiteral value: String) {
        self.rawValue = value
    }
    
    init(extendedGraphemeClusterLiteral value: String) {
        self.rawValue = value
    }
    
    init(unicodeScalarLiteral value: String) {
        self.rawValue = value
    }
    
}

// MARK: - Hashable
extension StringIdentifier: Hashable {
    var hashValue: Int {
        return self.rawValue.hash
    }
}


// MARK: - Equatable
extension StringIdentifier: Equatable {
    static func ==(lhs: StringIdentifier, rhs: StringIdentifier) -> Bool {
        return lhs.rawValue == rhs.rawValue
    }
}
