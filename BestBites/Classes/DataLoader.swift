//
//  DataLoader.swift
//  BestBites
//
//  Created by Aleksandr Zhovtyi on 5/3/18.
//  Copyright © 2018 Aleksandr Zhovtyi. All rights reserved.
//

import Foundation

final class DataLoader {
    private(set) var session: URLSession
    init(with session: URLSession = .shared) {
        self.session = session
    }
    
    @discardableResult
    func getData(from endpoint: Endpoint, then handler: ((_ json: JSON?, _ error: Error?) -> Void)?) -> RequestToken? {
        let task = session.dataTask(with: endpoint.request) { (data, response, error) in
            guard error == nil else {
                handler?(nil, error!)
                return
            }
            
            guard let data = data else {
                handler?([:], nil)
                return
            }
            
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                if let array = json as? [[String: Any]] {
                    let response = ["values": array]
                    DispatchQueue.main.async { handler?(response, nil) }
                } else {
                    let response = json as? [String: Any] ?? [:]
                    DispatchQueue.main.async { handler?(response, nil) }
                }
            } catch {
                DispatchQueue.main.async { handler?([:], nil) }
            }
            
        }
        let requestToken = RequestToken(task: task)
        task.resume()
        return requestToken
    }
    
    @discardableResult
    func sendData(to endpoint: Endpoint, then handler: ((_ error: Error?) -> Void)? = nil) -> RequestToken? {
        let task = session.dataTask(with: endpoint.request) { (data, response, error) in
            guard error == nil else {
                handler?(error!)
                return
            }
            handler?(nil)
            
        }
        let requestToken = RequestToken(task: task)
        task.resume()
        return requestToken
    }
}
