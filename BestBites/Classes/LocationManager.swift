//
//  LocationManager.swift
//  BestBites
//
//  Created by Aleksandr Zhovtyi on 5/7/18.
//  Copyright © 2018 Aleksandr Zhovtyi. All rights reserved.
//

import Foundation
import CoreLocation
import CoreData

final class LocationManager {
    var maximumRegionMonitoringDistance: Float = 2
    static let shared: CLLocationManager = {
        let manager = CLLocationManager()
        
        manager.activityType = .fitness
        manager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        manager.distanceFilter = 10
        
//        manager.desiredAccuracy = kCLLocationAccuracyHundredMeters // less batery ussage
        manager.pausesLocationUpdatesAutomatically = false
        manager.allowsBackgroundLocationUpdates = true
        
        
        return manager
    }()
    
    private init() { }
}

extension CLLocationManager {
    func stopMonitoring() {
        
    }
    
    func startMonitoring() {
        guard  UserDefaults.standard.isTrackingEnabled else { return }
        
        let request: NSFetchRequest<Place> = Place.fetchRequest()
//        request.resultType = .dictionaryResultType
//        request.returnsDistinctResults = true
//        request.propertiesToFetch = [#keyPath(Place.latitude), #keyPath(Place.longitude)]
        let moc = Database.default.viewContext
//        let results = try? moc.execute(request)
//        print(results)
//        print("👍 ", String(describing: type(of: self)),":", #function, " ", results.map)
        
//        for var in results {
//            print(val)
//        }
        
        let results = try? moc.fetch(request)
        for value in results ?? [] {
            monitorRegionAtLocation(center: value.location.coordinate, identifier: value.identifier)
        }
        
        
    }
    
    func monitorRegionAtLocation(center: CLLocationCoordinate2D, identifier: Place.Identifier ) {
        // Make sure the app is authorized.
        let status = CLLocationManager.authorizationStatus()
        if status == .authorizedAlways || status == .authorizedWhenInUse {
            // Make sure region monitoring is supported.
            if CLLocationManager.isMonitoringAvailable(for: CLCircularRegion.self) {
                // Register the region.
                let maxDistance = maximumRegionMonitoringDistance
                let region = CLCircularRegion(center: center, radius: maxDistance, identifier: String(format: "%d", identifier))
                region.notifyOnEntry = true
                region.notifyOnExit = false
                startMonitoring(for: region)
            }
        }
    }
}
