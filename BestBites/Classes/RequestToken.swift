//
//  RequestToken.swift
//  BestBites
//
//  Created by Aleksandr Zhovtyi on 5/3/18.
//Copyright © 2018 Aleksandr Zhovtyi. All rights reserved.
//

import Foundation

class RequestToken {
    private weak var task: URLSessionTask?


    init(task: URLSessionTask) {
        self.task = task
    }

    func cancel() {
        task?.cancel()
    }
}
