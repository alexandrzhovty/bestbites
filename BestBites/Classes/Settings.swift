//
//  Settings.swift
//  BestBites
//
//  Created by Aleksandr Zhovtyi on 5/3/18.
//  Copyright © 2018 Aleksandr Zhovtyi. All rights reserved.
//

import Foundation

struct Settings {
    private init() {}
}


extension Settings {
    static let startURL = URL(string: "https://bestbitesthailand.com")!
    
    static let apiServerURL:(scheme: String, host: String, path: String) = (
        "https", "bestbitesthailand.com", "/wp-json/wp/v2/"
    )
}
