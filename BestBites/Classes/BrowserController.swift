//
//  BrowserController.swift
//  BestBites
//
//  Created by Aleksandr Zhovtyi on 5/2/18.
//Copyright © 2018 Aleksandr Zhovtyi. All rights reserved.
//

import UIKit
import WebKit
import CoreLocation
import SafariServices

final class BrowserController: UIViewController {
    //    MARK: - Properties & variables
    //    MARK: Public
    
    //    MARK: Outlets
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var backButton: UIBarButtonItem!
    @IBOutlet weak var forwardButton: UIBarButtonItem!
    
    @IBOutlet weak var backBarButton: UIBarButtonItem!
    
    
    
    //    MARK: Private
    private var observerWasCreated: Bool = false
    
    //    MARK: - Initializations
//    class func instantiate() -> BrowserController {
//        let vc = Storyboard(<#T##storyboard: StroyboadType##StroyboadType#>).instantiateViewController(BrowserController.self)
//        return vc
//    }
    
    
    private var observerToken: Any?
    deinit {
        unregisterObserver()
    }

}

//    MARK: - View life cycle
extension BrowserController  {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        // Customize appearance
        Appearance.customize(viewController: self)
        
//        #if DEBUG
//        let url  = Bundle.main.url(forResource: "Temp", withExtension: "html")!
//        let request = URLRequest(url: url)
//        #else
        let request = URLRequest(url: Settings.startURL)
//        #endif
        
        
        webView.load(request)
        webView.scrollView.contentInsetAdjustmentBehavior = .never
        
        view.subviews.forEach(setup)
        view.subviews.forEach(configure)
        
        navigationItem.titleView = UIImageView(image: UIImage(named: "ic_logo"))
        backButton.isEnabled = false
        
//        let locationManager = LocationManager.shared
//        locationManager.delegate = AppDelegate.shared
//        locationManager.requestAlwaysAuthorization()
        
        registerObserver()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let url = AppDelegate.shared.launchURL {
            let request = URLRequest(url: url)
            webView.load(request)
        }
        AppDelegate.shared.launchURL = nil
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
}

extension BrowserController: ObserverProtocol {
    func registerObserver() {
        webView.addObserver(self, forKeyPath: #keyPath(WKWebView.estimatedProgress), options: .new, context: nil)
        webView.addObserver(self, forKeyPath: #keyPath(WKWebView.canGoBack), options: .new, context: nil)
        webView.addObserver(self, forKeyPath: #keyPath(WKWebView.canGoForward), options: .new, context: nil)
        
       observerToken =  AppDelegate.shared.observe(\AppDelegate.launchURL, options: [.new]) {
            [weak self] (_, _) in
            guard let `self` = self else { return }
            if let url = AppDelegate.shared.launchURL {
                self.showLaucnUrl(url)
            }
        
        }
        observerWasCreated = true
    }
    
    func unregisterObserver() {
        if observerWasCreated {
            webView.removeObserver(self, forKeyPath: "estimatedProgress")
            webView.removeObserver(self, forKeyPath: "canGoBack")
            webView.removeObserver(self, forKeyPath: "canGoForward")
            
        }
    }
    
    private func showLaucnUrl(_ url: URL) {
        let request = URLRequest(url: url)
        self.webView.load(request)
         AppDelegate.shared.launchURL = nil
    }
    
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        switch keyPath {
        case "estimatedProgress"?:
            self.progressView.isHidden = self.webView.estimatedProgress == 1
            self.progressView.progress = Float(webView.estimatedProgress)
            
        case "canGoBack"?:
            backButton.isEnabled = webView.canGoBack
            
        case "canGoForward"?:
            forwardButton.isEnabled = webView.canGoForward
            
        default:
            return super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
        }
    }
}

//    MARK: - Utilities
extension BrowserController  {
    // MARK: Displaying
    private func setup(_ viewToSetup: UIView) {
        switch viewToSetup {
        case progressView:
            progressView.progress = 0
    
        case webView:
            backButton.isEnabled = webView.canGoBack
            forwardButton.isEnabled = webView.canGoForward
            
            webView.navigationDelegate = self
            webView.uiDelegate = self

        default: break
        }
        
    }
    
    private func configure(_ viewToConfig: UIView) {
        switch viewToConfig {
//        case tableView:
//            break

            
        default: break
        }
        
    }

    // MARK: Buttons
    @IBAction func didTapBack(barButton: UIBarButtonItem) {
        
        webView.goBack()
    }
    
    @IBAction func didTapHome(barButton: UIBarButtonItem) {
        let request = URLRequest(url: Settings.startURL)
        webView.load(request)
    }
    
    @IBAction func didTapForward(barButton: UIBarButtonItem) {
        webView.goForward()
    }
    
    @IBAction func didTapRefresh(barButton: UIBarButtonItem) {
        webView.reload()
    }
}

extension BrowserController: WKUIDelegate {
    func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
        if navigationAction.targetFrame == nil {
            webView.load(navigationAction.request)
        }
        return nil
    }
    
}

extension BrowserController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
//        let url = navigationAction.request.url
//        let urlString = (url != nil) ? url!.absoluteString : ""
//
//
//        let appStorePattern = "\\/\\/itunes\\.apple\\.com\\/"
//        if let regex = try? NSRegularExpression(pattern: appStorePattern, options: [.caseInsensitive]) {
//            let matches = regex.matches(in: urlString, options: [], range: NSMakeRange(0, urlString.count))
//            if matches.count > 0 {
//                if let url = url, UIApplication.shared.canOpenURL(url) {
//                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
//                }
//                decisionHandler(.cancel)
//                return
//            }
//        }
        
        decisionHandler(.allow)
        
    }
}
