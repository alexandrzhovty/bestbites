//
//  Image.swift
//  BestBites
//
//  Created by Aleksandr Zhovtyi on 5/2/18.
//Copyright © 2018 Aleksandr Zhovtyi. All rights reserved.
//

import UIKit


typealias Image = UIImage



//typealias Background = UIImage.backgorund
//typealias Logo = UIImage.logo
//typealias Icon = UIImage.icon



// MARK: - Image from color
extension Image {
    convenience init?(color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        guard let cgImage = image?.cgImage else { return nil }
        self.init(cgImage: cgImage)
    }
}

// MARK: - Gradient
extension Image {

    enum GradientDirection {
        case horizontal
    }

    struct GradientPoint {
        var location: CGFloat
        var color: UIColor
    }

    convenience init?(size: CGSize, startColor: UIColor, endColor: UIColor, gradientDirection: Image.GradientDirection) {
        UIGraphicsBeginImageContextWithOptions(size, false, UIScreen.main.scale)



        guard let context = UIGraphicsGetCurrentContext() else { return nil }       // If the size is zero, the context will be nil.
        guard let gradient = CGGradient(colorsSpace: CGColorSpaceCreateDeviceRGB(), colors: [startColor.cgColor, endColor.cgColor] as CFArray, locations: [0, 0.4])
            else { return nil }

        let position: (start: CGPoint, end: CGPoint)

        switch gradientDirection {
        case .horizontal: position = (CGPoint.zero, CGPoint(x: size.width, y: 0))
        }

        context.drawLinearGradient(gradient, start: position.start, end: position.end, options: CGGradientDrawingOptions())
        guard let image = UIGraphicsGetImageFromCurrentImageContext()?.cgImage else { return nil }
        self.init(cgImage: image)
        defer { UIGraphicsEndImageContext() }

    }
}
