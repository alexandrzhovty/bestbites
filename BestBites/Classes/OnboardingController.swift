//
//  OnboardingController.swift
//  BestBites
//
//  Created by Aleksandr Zhovtyi on 5/3/18.
//Copyright © 2018 Aleksandr Zhovtyi. All rights reserved.
//

import UIKit
import CoreData
import CoreLocation

final class OnboardingController: UIViewController {
    //    MARK: - Properties & variables
    var viewContext: NSManagedObjectContext
    
    //    MARK: Public
    
    //    MARK: Outlets
	@IBOutlet weak var tableView: UITableView!
    
    //    MARK: Private
    private(set) var entities: (loader: DataLoader, requestToken: RequestToken?)
    private var locationManager: CLLocationManager?
    
    //    MARK: - Initializations
    class func instantiate() -> OnboardingController {
        let vc = Storyboard(.onboarding).instantiateViewController(OnboardingController.self)
        return vc
    }

    required init?(coder aDecoder: NSCoder) {
        entities = (DataLoader(), nil)
        viewContext = Database.default.viewContext
        super.init(coder: aDecoder)
    }

    deinit {
        print("👍 ", String(describing: type(of: self)),":", #function)
//        printDeinit()
    }
}

//    MARK: - View life cycle
extension OnboardingController  {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        // Customize appearance
        Appearance.customize(viewController: self)
        
        view.subviews.forEach(setup)
        view.subviews.forEach(configure)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
        let fetchRequest: NSFetchRequest<Place> = Place.fetchRequest()
        let count = (try? viewContext.count(for: fetchRequest)) ?? 0
        if count > 0 /* && UserDefaults.standard.isFirstLauch == false */ {
            Database.default.loadData()
            goInside()
        } else {
            Database.default.loadData() {
                [weak self] in self?.locationAccess()
            }
        }
        
        
        
//        loadDataFromServer()
    }
}

//    MARK: - Utilities
extension OnboardingController  {
    // MARK: Displaying
    private func setup(_ viewToSetup: UIView) {
        switch viewToSetup {
//        case tableView:
//            tableView.estimatedRowHeight = 44
//            tableView.rowHeight = UITableViewAutomaticDimension


        default: break
        }
        
    }
    
    private func configure(_ viewToConfig: UIView) {
        switch viewToConfig {
//        case tableView:
//            break

            
        default: break
        }
        
    }
    
    // MARK: Navigation
    private func goInside() {
        UserDefaults.standard.isFirstLauch = false
       
        
        let vc = Storyboard(.main).instantiateInitialViewController()!
//        let window = AppDelegate.shared.window
//        window?.rootViewController = vc
        let to = UIWindow.TransitionOptions(direction: .fade, style: .linear)
        UIApplication.shared.keyWindow?.setRootViewController(vc, options: to)
    }

    // MARK: Network

    private func loadDataFromServer(page pageNumber: Int = 0) {
        entities.requestToken?.cancel()
        
        let endpoint = Endpoint.locations(pageNumber: 1)
        weak var weakSelf = self
        entities.requestToken = entities.loader.getData(from: endpoint, then: { weakSelf?.render(response: $0, error: $1, page: pageNumber) })

    }

    private func render(response json: JSON?, error: Error?, page pageNumber: Int ) {
        guard error == nil, let json = json?["values"] as? [JSON] else {
            return
        }
        
        let moc = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        moc.parent = viewContext
        moc.perform {
            if pageNumber == 1 {
                Place.removeMissingEnities(in: json, from: moc, matching: nil)
                if moc.hasChanges { try? moc.save() }
            }
            
            Place.parse(entities: json, into: moc)
            if moc.hasChanges { try? moc.save() }
            DispatchQueue.main.async {
                [weak self] in self?.goInside()
            }
        }
        
//        let values = json?["values"] as? [JSON]
//        if let value = values?.first {
//            let location = value[""]
//        }
//        print("👍 ", String(describing: type(of: self)),":", #function, " ", json)
    }
    
    // MARK: Grand access methods
    func locationAccess() {
        
        
        
//        guard CLLocationManager.locationServicesEnabled() == false else {
//            AppDelegate.shared.startMonitoring()
//            notificationAccess()
//            return
//        }
        
        let vc = ConfirmController.instantiate()
        vc.view.awakeFromNib()
        vc.titleLabel.text = NSLocalizedString("Would like use your location for to show you nearby BestBites Thailand Members", comment: "")
        vc.confirmAction = {
            self.locationManager = CLLocationManager()
            self.locationManager?.delegate = self
            self.locationManager!.requestAlwaysAuthorization()
            
            
            vc.remove()
        }
        vc.cancelAction = {
            vc.remove()
            self.notificationAccess()
        }
        add(vc)
        
    }
    
    func notificationAccess() {
        let vc = ConfirmController.instantiate()
        vc.view.awakeFromNib()
        vc.titleLabel.text = NSLocalizedString("BestBites wants to send you notifications with vouchers and invitations", comment: "")
        vc.confirmAction = {
            AppDelegate.shared.registerForPushNotifications(UIApplication.shared)
            
            self.goInside()
            vc.remove()
        }
        vc.cancelAction = {
            self.goInside()
        }
        add(vc)
    }
}

extension OnboardingController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status != .notDetermined {
            AppDelegate.shared.startMonitoring()
            notificationAccess()
        }
    }
}


