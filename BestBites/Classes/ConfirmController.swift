//
//  ConfirmController.swift
//  BestBites
//
//  Created by Aleksandr Zhovtyi on 8/20/18.
//Copyright © 2018 Aleksandr Zhovtyi. All rights reserved.
//

import UIKit


final class ConfirmController: UIViewController {
    //    MARK: - Properties & variables
    //    MARK: Public
    
    //    MARK: Outlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var confirmButton: UIButton!
    
    @IBOutlet weak private var contentView: UIView!
    @IBOutlet weak private var wrapperView: UIView!
    
    
    var confirmAction: (() -> Void)?
    var cancelAction: (() -> Void)?
    
    //    MARK: Private
    
    //    MARK: - Initializations
    class func instantiate() -> ConfirmController {
        let vc = Storyboard(.onboarding).instantiateViewController(ConfirmController.self)
        return vc
    }

    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
    }
}


//    MARK: - View life cycle
extension ConfirmController  {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        // Customize appearance
        Appearance.customize(viewController: self)
        
        view.subviews.forEach(setup)
        view.subviews.forEach(configure)
    }
}

//    MARK: - Utilities
extension ConfirmController  {
    // MARK: Displaying
    private func setup(_ viewToSetup: UIView) {
        switch viewToSetup {
        case wrapperView:
            wrapperView.layer.cornerRadius = 2
            wrapperView.clipsToBounds = true
            wrapperView.addShadow()

        default: break
        }
        
    }
    
    private func configure(_ viewToConfig: UIView) {
        switch viewToConfig {
//        case tableView:
//            break

            
        default: break
        }
        
    }
    
    // MARK: Buttons
    @IBAction func didTapAccept(button: UIButton) {
        confirmAction?()
    }

    @IBAction func didTapCancel(button: UIButton) {
        cancelAction?()
    }

}


