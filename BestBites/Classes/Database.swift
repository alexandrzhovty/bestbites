//
//  Database.swift
//  BestBites
//
//  Created by Aleksandr Zhovtyi on 5/4/18.
//Copyright © 2018 Aleksandr Zhovtyi. All rights reserved.
//

import CoreData

class Database {
    static let `default` = Database(inMemory: false)
    private(set) var modelName: String
    private(set) var inMemory: Bool
    
    private(set) var entities: (loader: DataLoader, requestToken: RequestToken?)
    
    var viewContext: NSManagedObjectContext {
        return persistentContainer.viewContext
    }

    /// Creates a container using the model named `name` in the main bundle
    ///
    /// - Parameter modelName: model name
    init(modelName: String = Bundle.main.infoDictionary!["CFBundleName"] as! String, inMemory: Bool = false) {
        entities = (DataLoader(), nil)
        self.modelName = modelName
        self.inMemory = inMemory
    }



    // MARK:  Core Data stack
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: modelName)


        if inMemory {
            let description = NSPersistentStoreDescription()
            description.type = NSInMemoryStoreType
            description.shouldAddStoreAsynchronously = false
            container.persistentStoreDescriptions = [description]
        }

        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error  \(error.localizedDescription)")
            }
        })

        return container
    }()


}

extension Database {
    func loadData(page pageNumber: Int = 0, then completion: (() -> Void)? = nil) {
        entities.requestToken?.cancel()
        
        
        
        let endpoint = Endpoint.locations(pageNumber: 1)
        entities.requestToken = entities.loader.getData(from: endpoint, then: {
            [weak self] (json, error) in
            
            defer { completion?() }
            
            guard let `self` = self else { return }
            
            guard error == nil, let json = json?["values"] as? [JSON] else {
                return
            }
            
//            let moc = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
            let moc = Database.default.viewContext
//            moc.parent = self.viewContext
            moc.performAndWait {
//                if pageNumber == 1 {
//                    Place.removeMissingEnities(in: json, from: moc, matching: nil)
//                    if moc.hasChanges { try? moc.save() }
//                }
                
                Place.parse(entities: json, into: moc)
                if moc.hasChanges { try? moc.save() }
                
            }
            
//            if self.viewContext.hasChanges {
//                self.viewContext.performAndWait {
//                    try? self.viewContext.save()
//                }
//            }
            LocationManager.shared.startMonitoring()
            
            
        
        })
    }
}
