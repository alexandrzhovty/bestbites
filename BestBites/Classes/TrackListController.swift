//
//  TrackListController.swift
//  BestBites
//
//  Created by Aleksandr Zhovtyi on 5/7/18.
//  Copyright © 2018 Aleksandr Zhovtyi. All rights reserved.
//

import UIKit
import CoreData
import CoreLocation
import MapKit


//    MARK: - Distatnce formatter
extension MKDistanceFormatter {
    ///  The default application Distance Formatter
    static var `default`: MKDistanceFormatter = MKDistanceFormatter()
}

class TrackListController: UITableViewController {
    
    var viewContext:  NSManagedObjectContext = Database.default.viewContext
    lazy var regions: [CLRegion] = prepareDataSource()
    
    
    
    private lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .none
        dateFormatter.timeStyle = .medium
        return dateFormatter
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    func prepareDataSource() -> [CLRegion] {
        return LocationManager.shared.monitoredRegions.sorted(by: { $0.identifier > $1.identifier })
    }
    
    @IBAction func didTapDelete(_ sender: Any) {
        let request: NSFetchRequest<Track> = Track.fetchRequest()
        let results = (try? viewContext.fetch(request)) ?? []
        for obj in results {
            viewContext.delete(obj)
        }
        if viewContext.hasChanges {
            try? viewContext.save()
        }
        
    }
    
    @IBAction func didTapRefresh(_ sender: Any) {
        self.regions = prepareDataSource()
        tableView.reloadData()
    }
}


// MARK: - Table view
extension TrackListController {
    // MARK: Data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        let sectionInfo = fetchedResultController.sections![section]
//        return sectionInfo.numberOfObjects
        return regions.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
    }
    
    // MARK: Displaying
    func configure(_ cell: UITableViewCell, at indexPath: IndexPath) {
        
        let region = regions[indexPath.row]
        
        let place = Place.by(region.identifier, from: viewContext)
        cell.textLabel?.text = place?.title
        if let location = LocationManager.shared.location, let place = place {
            let distance = location.distance(from: place.location)
            cell.detailTextLabel?.text = MKDistanceFormatter.default.string(fromDistance: distance)
        } else {
            cell.detailTextLabel?.text = nil
        }
        
//        let place = fetchedResultController.object(at: indexPath)
//
//        cell.textLabel?.text = dateFormatter.string(from: place.created!)
//        cell.detailTextLabel?.text = String(format: "%f : %f", place.latitude, place.longitude)
//
        
//        print("👍 ", String(describing: type(of: self)),":", #function, " ", place.created!, " = ", dateFormatter.string(from: place.created!))
//            cell.textLabel?.text = "asdfasdfasdf"
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        configure(cell, at: indexPath)
    }
}


