//
//  Defaults.swift
//  BestBites
//
//  Created by Aleksandr Zhovtyi on 5/7/18.
//  Copyright © 2018 Aleksandr Zhovtyi. All rights reserved.
//

import Foundation

extension UserDefaults {
    
    func registerDefaults() {
        let settings: [String: Any] = [
            #keyPath(UserDefaults.isTrackingEnabled): true,
            #keyPath(UserDefaults.isFirstLauch): true,
        ]
        register(defaults: settings)
    }
    
    @objc var isFirstLauch: Bool {
        get { return bool(forKey: #function) }
        set { set(newValue, forKey: #function) }
    }
    
    @objc var isTrackingEnabled: Bool {
        get { return bool(forKey: #function) }
        set { set(newValue, forKey: #function) }
    }
}
