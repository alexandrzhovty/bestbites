//
//  Appearance.swift
//  BestBites
//
//  Created by Aleksandr Zhovtyi on 5/2/18.
//Copyright © 2018 Aleksandr Zhovtyi. All rights reserved.
//

import UIKit

final class Appearance {
    
    //    MARK: - Class functions
    class func customize() {
        
//        UIApplication.shared.keyWindow?.tintColor  = Color.tint
        Appearance.customizeTabBar()
        Appearance.customizeNavigationBar()
    }
    
    private class func customizeTabBar() {
//        let tabBar = UITabBar.appearance()
//        tabBar.tintColor = UIColor.white
//        tabBar.backgroundImage = UIImage(color: Color.tint)!
////        tabBar.shadowImage = UIImage(color: .clear)!
//        //        let size = CGSize(width: 3, height: 3)
//        //        let start = UIColor.clear
//        //        let end = UIColor.black.withAlphaComponent(0.1)
//        //        let image = Image(size: size, startColor: start, endColor: end, gradientDirection: .horizontal)
//        //        let image = UIImage(named: "TabBar-Shadow")
//        //        tabBar.shadowImage = image
//        
//        
        let toolBar = UIToolbar.appearance()
//        toolBar.barTintColor = Color.tint
        toolBar.tintColor = .white
    }
    
    class private func customizeNavigationBar() {
        let navBar = UINavigationBar.appearance()
        
//        // Navigation bar title
        let attr: [NSAttributedString.Key : Any] = [
            NSAttributedString.Key.foregroundColor: Color.white
        ]
        navBar.titleTextAttributes = attr
        
        navBar.tintColor = UIColor.white
//        navBar.barTintColor = UIColor.tint
        
        
        // Background color
//        navBar.isTranslucent = false
        navBar.setBackgroundImage(UIImage(named: "Navigation-Bkg")!, for: .default)
//        navBar.shadowImage = UIImage(color: .clear)
        
        //        // Back color #imageLiteral(resourceName: "btn_back")
//        navBar.backIndicatorImage = #imageLiteral(resourceName: "Icon-Back")
//        navBar.backIndicatorTransitionMaskImage = #imageLiteral(resourceName: "Icon-Back")
        
    }
    
    
    class func customize(viewController: UIViewController) {
        
    }
}
