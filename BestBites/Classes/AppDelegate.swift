//
//  AppDelegate.swift
//  BestBites
//
//  Created by Aleksandr Zhovtyi on 5/2/18.
//  Copyright © 2018 Aleksandr Zhovtyi. All rights reserved.
//

import UIKit
import CoreData
import CoreLocation
import UserNotifications
import SafariServices


@UIApplicationMain
final class AppDelegate: UIResponder {
    var window: UIWindow?
    var lastLocation: CLLocation = CLLocation(latitude: 0, longitude: 0)
    var qty: Int = 0
    static var shared: AppDelegate { return UIApplication.shared.delegate as! AppDelegate }
    var viewContext: NSManagedObjectContext = Database.default.viewContext
    @objc dynamic var launchURL: URL? = nil
    
}

// MARK: - Application delegate
extension AppDelegate: UIApplicationDelegate {
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        
        UserDefaults.standard.register(defaults: ["UserAgent": "Mozilla/5.0 (iPhone; CPU iPhone OS 8_2 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) Mobile/12D508 [FBAN/FBIOS;FBAV/27.0.0.10.12;FBBV/8291884;FBDV/iPhone7,1;FBMD/iPhone;FBSN/iPhone OS;FBSV/8.2;FBSS/3; FBCR/vodafoneIE;FBID/phone;FBLC/en_US;FBOP/5]"])
        
        Appearance.customize()
        
        let userDefaults = UserDefaults.standard
        userDefaults.registerDefaults()
        print(userDefaults.isTrackingEnabled)
        registerObserver()
        
        
        
        if userDefaults.isFirstLauch == false {
            registerForPushNotifications(application)
            startMonitoring()
        }
        
        
        
        
        return true
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        unregisterObserver()
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        qty = 0
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
    
}


var appDelegateContext = 123654
extension AppDelegate: ObserverProtocol {
    func registerObserver() {
        let defaults = UserDefaults.standard
        defaults.addObserver(self, forKeyPath: #keyPath(UserDefaults.isTrackingEnabled), options: [.new], context: &appDelegateContext)
    }
    
    func unregisterObserver() {
        let defaults = UserDefaults.standard
        defaults.removeObserver(self, forKeyPath: #keyPath(UserDefaults.isTrackingEnabled), context: &appDelegateContext)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        switch keyPath {
        case #keyPath(UserDefaults.isTrackingEnabled) where context == &appDelegateContext:
            if UserDefaults.standard.isTrackingEnabled {
                startLocationUpdates()
            } else {
                stopLocationUpdates()
            }
        default:
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
        }
    }
}

extension AppDelegate: CLLocationManagerDelegate {
    func startLocationUpdates() {
        
        
        
        DispatchQueue.main.async {
            let locationManager = LocationManager.shared
            locationManager.delegate = self
            locationManager.startUpdatingLocation()
            guard let location = locationManager.location else { return }
            self.startMonitoring(with: locationManager, for: location)
//            locationManager.startMonitoring()
        }
        
        
//        locationManager.startMonitoringSignificantLocationChanges()
    }
    
    func stopLocationUpdates() {
        DispatchQueue.main.async {
            let locationManager = LocationManager.shared
            locationManager.stopUpdatingLocation()
            self.stopMonitoring(with: locationManager)
            
            
            let center = UNUserNotificationCenter.current()
            center.removeAllPendingNotificationRequests()
        }
        
        
    }
    
    func startMonitoring(with locationManager: CLLocationManager, for location: CLLocation) {
       
        
        let searchDistance: CLLocationDistance =  10_000.0
        let regionRadius: CLLocationDistance = 100
        
        
        let region = CLCircularRegion(center: location.coordinate, radius: searchDistance, identifier: UUID().uuidString)
        
        func deg2rad(_ degrees: Double) -> Double {
//            return degrees * .pi / 180
            return Measurement(value: degrees, unit: UnitAngle.degrees).converted(to: .radians).value
        }
        
        
        let minLat = location.coordinate.latitude - (searchDistance / 69.0);
        let maxLat = location.coordinate.latitude + (searchDistance / 69.0);
        let minLon = location.coordinate.latitude - searchDistance / fabs(cos(deg2rad(location.coordinate.latitude))*69);
        let maxLon = location.coordinate.longitude + searchDistance / fabs(cos(deg2rad(location.coordinate.latitude))*69);
        
        let predicate = NSPredicate(format: "latitude <= %f AND latitude >= %f AND longitude <= %f AND longitude >= %f", maxLat, minLat, maxLon, minLon)
//        let pred = NSPredicate(format: "NOT (%K in %@)", #keyPath(Place.id), ["adfa", "asfad"])
//        
//        let compound = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate, pred])
        
        
        
        let moc = Database.default.viewContext
        let request: NSFetchRequest<Place> = Place.fetchRequest()
        request.predicate = predicate
        let results: [Place]
        do { results = try moc.fetch(request) }
        catch { return }
        
        var places: [(place: Place, distance: Double)] = []
        
        for place in results {
            guard region.contains(place.location.coordinate) else { continue }
            let distance = location.distance(from: place.location)
            places.append((place, distance))
        }
        
        
        // Sorting
        places.sort (by: { $0.distance < $1.distance })
        
        // Only 20 places can be monitored
        let maxPlaceQtyToMonitor = 20
        if places.count > maxPlaceQtyToMonitor {
            places.removeSubrange(maxPlaceQtyToMonitor...)
        }
        
        
        // Stop monitoring regions missing in places
//        let ids: Set<String> = Set(places.map { $0.place.identifier })
//        let regionsToStop = locationManager.monitoredRegions.filter{ !ids.contains($0.identifier) }
//        regionsToStop.forEach{ locationManager.stopMonitoring(for: $0) }
        
        let regionsToStop = locationManager.monitoredRegions
        
        // Start monitoring regions
//        let alreadyStartedIds: Set<String> = Set(locationManager.monitoredRegions.map{ $0.identifier })
        
        let center = UNUserNotificationCenter.current()
        if regionsToStop.count > 0 {
            center.removePendingNotificationRequests(withIdentifiers: regionsToStop.map{ $0.identifier })
        }
        regionsToStop.forEach{ locationManager.stopMonitoring(for: $0) }

        
        for place in places.map({ $0.place }) {
//            guard !alreadyStartedIds.contains(place.identifier) else { continue }
            let region = CLCircularRegion(center: place.location.coordinate, radius: regionRadius, identifier: place.identifier)
            region.notifyOnEntry = true
            region.notifyOnExit = true
            locationManager.startMonitoring(for: region)
            
            
            // Location notification trigger
            let content = UNMutableNotificationContent()
            content.title = place.title ?? "New item"
            content.body = place.title ?? "New item"
            content.categoryIdentifier = actionCategoryIdentifier
            content.sound = UNNotificationSound.default
            
            if let link = place.link?.absoluteString {
                content.userInfo = ["link": link]
            }
            
            let trigger = UNLocationNotificationTrigger(region: region, repeats: false)
            
            let request = UNNotificationRequest(identifier: place.identifier, content: content, trigger: trigger)
//            center.add(request)
            
            center.add(request) { (error) in
                guard let error = error else {
                    print("EVERYTHING is OK")
                    return
                }
                print("cannot add request because of \(error)")
            }
        }
        
    }
    
    func stopMonitoring(with locationManager: CLLocationManager) {
        locationManager.monitoredRegions.forEach(locationManager.stopMonitoring)
    }
    
    
    func startMonitoring() {
        DispatchQueue.main.async {
            
            let locationManager = LocationManager.shared
            locationManager.delegate = self
            if CLLocationManager.locationServicesEnabled() {
                //            locationManager.distanceFilter = kCLDistanceFilterNone
                
//                locationManager.requestAlwaysAuthorization()
                let userDefaults = UserDefaults.standard
                if userDefaults.isTrackingEnabled {
                    self.startLocationUpdates()
                    if let location = locationManager.location {
                        self.startMonitoring(with: locationManager, for: location)
                    }
                }
            }
        }
    }

    
    func escalateLocationServiceAuthorization() {
//        // Escalate only when the authorization is set to when-in-use
//        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
//            LocationManager.shared.requestAlwaysAuthorization()
//           afd
//        }
        
    }
    
//    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
//        Alert.default.showOk("WE ARE HERE", message: region.identifier)
//    }
//
//    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
//        Alert.default.showOk("WE ARE HERE", message: region.identifier)
//    }
    
    func locationManagerDidPauseLocationUpdates(_ manager: CLLocationManager) {
        
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            break
        case .restricted:
            break
        case .denied:
            break
        case .authorizedAlways:
            break
        case .authorizedWhenInUse:
            startLocationUpdates()
            escalateLocationServiceAuthorization()
        }

        if case .authorizedAlways = status  {
            startLocationUpdates()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locationManager = LocationManager.shared
        guard let location = locations.last else { return }
        if location.distance(from: lastLocation) > 50 {
            startMonitoring(with: locationManager, for: location)
        }
        lastLocation = location
    }
    
    
}

// MARK: - Notifications
let repeatActionIdentifier = "REPEAT_ACTION_IDENTIFIER"
let openLinkActionIdentifier = "OPEN_LINK_ACTION_IDENTIFIER"
let actionCategoryIdentifier = "ACTION_CATEGORY_IDENTIFIER"

extension AppDelegate: UNUserNotificationCenterDelegate {
    func registerForPushNotifications(_ application: UIApplication = .shared) {
        UNUserNotificationCenter.current().delegate = self
        
        let authOptions: UNAuthorizationOptions = [.alert, .sound, .badge]
        UNUserNotificationCenter.current().requestAuthorization(options: authOptions) {
            (granted, error) in
            guard granted else { return }
            
//            let repeatAction = UNNotificationAction(identifier: repeatActionIdentifier, title:"Repeat",options:[])
            
//            let linkAction = UNNotificationAction(identifier: openLinkActionIdentifier, title: "Open in Safari", options: [])
            
            let category = UNNotificationCategory(identifier: actionCategoryIdentifier,
                                                  actions: [],
                                                  intentIdentifiers: [], options: [])
            
            UNUserNotificationCenter.current().setNotificationCategories([category])
            
            
            
            self.getNotificationSettings()
            
           
            
        }
        
    }
    
    func getNotificationSettings() {
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            guard settings.authorizationStatus == .authorized else { return }
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let token = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
        print("👍 ", String(describing: type(of: self)),":", #function, " token: ", token)
        let endpont = Endpoint.register(token: token)
        let dataLoader = DataLoader()
        dataLoader.sendData(to: endpont)
    }
    
    
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([ .sound, .alert ])
    }
    
    
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        
        var link = response.notification.request.content.userInfo["link"] as? String
        if link == nil {
            let input = response.notification.request.content.body
            let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
            let matches = detector.matches(in: input, options: [], range: NSRange(location: 0, length: input.utf16.count))
            
            for match in matches {
                guard let range = Range(match.range, in: input) else { continue }
                link = String(input[range])
            }
        }
        
        
        func open(link: String?) {
            if let link = link, let url = URL(string: link), UIApplication.shared.canOpenURL(url) {
//                UIApplication.shared.open(url, options: [:], completionHandler: nil)
                
                self.launchURL = url
            }
        }
        
        switch response.actionIdentifier {
        case UNNotificationDefaultActionIdentifier:
            open(link: link)
        default:
            open(link: link)
        }
        completionHandler()
        
        
    }
    
    func addNotification(content:UNNotificationContent,trigger:UNNotificationTrigger?, indentifier:String){
        let request = UNNotificationRequest(identifier: indentifier, content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request, withCompletionHandler: {
            (errorObject) in
            if let error = errorObject{
                print("Error \(error.localizedDescription) in notification \(indentifier)")
            }
        })
    }
}


