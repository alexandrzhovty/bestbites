//
//  PlaceListController.swift
//  BestBites
//
//  Created by Aleksandr Zhovtyi on 5/4/18.
//  Copyright © 2018 Aleksandr Zhovtyi. All rights reserved.
//

import UIKit
import CoreData
import CoreLocation

class PlaceListController: UITableViewController {
    
    var viewContext:  NSManagedObjectContext = Database.default.viewContext
    
    private var _fetchedResultController: NSFetchedResultsController<Place>? = nil
    lazy var fetchedResultController: NSFetchedResultsController<Place> = {
        guard _fetchedResultController == nil else { return _fetchedResultController! }
        
        let fetchRequest: NSFetchRequest<Place> = Place.fetchRequest()
        let sortByOrder = NSSortDescriptor(key: #keyPath(Place.id), ascending: false)
        
        fetchRequest.sortDescriptors = [sortByOrder]
        fetchRequest.fetchBatchSize = 10
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: viewContext, sectionNameKeyPath: nil, cacheName: nil)
        fetchedResultsController.delegate = self
        
        try? fetchedResultsController.performFetch()
        return fetchedResultsController
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    @IBAction func didTapAdd(_ sender: UIBarButtonItem) {
        // 1. Current location
        let status = CLLocationManager.authorizationStatus()
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            break
        default: return
        }
        
        guard let location = LocationManager.shared.location else { return }
        
        
        // 2. Reverse geocoding
        let geocoder = CLGeocoder()
        
//        // Look up the location and pass it to the completion handler
//        geocoder.reverseGeocodeLocation(lastLocation,
//                                        completionHandler: { (placemarks, error) in
//                                            if error == nil {
//                                                let firstLocation = placemarks?[0]
//                                                completionHandler(firstLocation)
//                                            }
//                                            else {
//                                                // An error occurred during geocoding.
//                                                completionHandler(nil)
//                                            }
//        })
        
        geocoder.reverseGeocodeLocation(location) {
            [weak self] (placemarks, error) in
            guard error == nil else { return }
            guard let firstPlace = placemarks?.first else { return }
            let address: [String?] = [firstPlace.thoroughfare, firstPlace.subThoroughfare, firstPlace.locality]
            
            
            guard let moc = self?.viewContext else { return }
            moc.perform {
                let place = Place(context: moc)
                place.identifier = "\(Date().timeIntervalSince1970)"
                place.title = address.compactMap{ $0 }.joined(separator: ", ")
                place.location = location
                
                if moc.hasChanges { try? moc.save() }
                guard UserDefaults.standard.isTrackingEnabled else { return }
                LocationManager.shared.monitorRegionAtLocation(center: location.coordinate, identifier: place.identifier)
                
            }
            
        }
        
        
        // 3. Store
        // 4. Start monitoring region
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

// MARK: - Table view
extension PlaceListController {
    // MARK: Data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return fetchedResultController.sections?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionInfo = fetchedResultController.sections![section]
        return sectionInfo.numberOfObjects
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
    }
    
    // MARK: Displaying
    func configure(_ cell: UITableViewCell, at indexPath: IndexPath) {
        let place = fetchedResultController.object(at: indexPath)
        cell.textLabel?.text = String(format: "%lld - %@", place.id, place.title ?? "")
        cell.detailTextLabel?.text = place.link?.absoluteString
        
        
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        configure(cell, at: indexPath)
    }
}

// MARK: - Fetched results controller
extension PlaceListController: NSFetchedResultsControllerDelegate {
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        switch type {
        case .insert:   tableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:   tableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        default:        return
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:   tableView.insertRows(at: [newIndexPath!], with: .fade)
        case .delete:   tableView.deleteRows(at: [indexPath!], with: .fade)
        case .update:
            guard let indexPath = indexPath, let cell = tableView.cellForRow(at: indexPath) else { return }
            configure(cell, at: indexPath)
        case .move:
            guard let indexPath = indexPath, let cell = tableView.cellForRow(at: indexPath) else { return }
            configure(cell, at: indexPath)
            tableView.moveRow(at: indexPath, to: newIndexPath!)
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
}
