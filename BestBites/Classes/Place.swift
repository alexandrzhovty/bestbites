//
//  Place.swift
//  BestBites
//
//  Created by Aleksandr Zhovtyi on 5/4/18.
//  Copyright © 2018 Aleksandr Zhovtyi. All rights reserved.
//

import CoreData
import CoreLocation



// MARK: Computed variables
extension Place {
    typealias Identifier = String
    var identifier: Identifier {
        get { return String(format: "%lld", id) }
        set { id = Int64(newValue) ?? 0 }
    }
    
    var location: CLLocation {
        get { return CLLocation(latitude: latitude, longitude: longitude) }
        set {
            latitude = newValue.coordinate.latitude
            longitude =  newValue.coordinate.longitude
        }
    }
}

// MARK: Parsing
extension Place {
    
    static var patterns: [String: NSRegularExpression] = [:]
    
    final class func parse(entity json: JSON, into context: NSManagedObjectContext) -> Place? {
        guard let id = json["id"] as? Place.Identifier else { return nil }
        
        let place = Place.by(id, from: context) ?? {
            let place = Place(context: context)
            place.identifier = id
            return place
            }()
        
        place.parse(json)
        return place
    }
    
    @discardableResult
    final class func parse(entities json: [JSON], into context: NSManagedObjectContext) -> [Place] {
        guard json.count > 0 else { return [] }
        
        let ids = json.map{ $0["id"] }.compactMap{ $0 }
 
        let request: NSFetchRequest<Place> = Place.fetchRequest()
        request.predicate = NSPredicate(format: "%K in %@", #keyPath(Place.id), ids)

        let results = (try? context.fetch(request)) ?? []
        
        var existing: [Int64: Place] = [:]
        for place in results {
            existing[place.id] = place
        }
        
        var returnValue = [Place]()
        
        for value in json {
            guard let id = value["id"] as? Int64 else { continue }
            
            let entity: Place = existing[id] ?? {
                let place = Place(context: context)
                place.id = id
                return place
            }()
            entity.parse(value)
            returnValue.append(entity)
            
        }
        
        return returnValue
        
    }
    
    
    
    static func by(_ identifier: Place.Identifier, from moc: NSManagedObjectContext) -> Place? {
        let request: NSFetchRequest<Place> = Place.fetchRequest()
        request.predicate = NSPredicate(format: "%K == %@", #keyPath(Place.id), identifier)
        request.fetchLimit = 1
        do {
            return try moc.fetch(request).first
        }
        catch { return nil }
    }
    
    final func parse(_ json: JSON) {
        if let s = json["title"] as? String {
            title = String(htmlEncodedString: s)
        } else {
           title = nil
        }
        
        if let address = json["link"] as? String, var escapedAddress = address.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
            self.link = URL(string: escapedAddress)
        } else {
            self.link = nil
        }
        
        
//        print("👍 ", String(describing: type(of: self)),":", #function, " self.link ", self.link)
//        
//        
//        print("WE ARE EHRE")
        func location(forKey key: String, in string: String) -> Double {
            let pattern = "\"\(key)\";s:\\d+:\"[^\"]+\""
            
            
            let regex: NSRegularExpression
            if let regEx = Place.patterns[pattern] {
                regex = regEx
            } else {
                guard let regEx = try? NSRegularExpression(pattern: pattern, options: [.caseInsensitive]) else { return 0.0 }
                regex = regEx
                Place.patterns[pattern] = regex
            }
            
            let matches = regex.matches(in: string, options: [], range: NSMakeRange(0, string.count))
            guard let range = matches.map({ Range($0.range, in: string) }).first else { return 0.0 }
            
            
            let substr = String(string[range!])
            
            guard substr.count > 0 else { return 0.0 }
            
            let subpattern = "\\d+(?:\\.\\d+)"
            let subregex: NSRegularExpression
            if let reg = Place.patterns[subpattern] {
                subregex = reg
            } else {
                guard let reg = try? NSRegularExpression(pattern: subpattern, options: [.caseInsensitive]) else { return 0.0 }
                subregex = reg
                Place.patterns[subpattern] = subregex
            }
            
            let submatches = subregex.matches(in: substr, options: [], range: NSMakeRange(0, substr.count))
            guard let subrange = submatches.map({ Range($0.range, in: string) }).first else { return 0.0 }
            
            
            let found = String(substr[subrange!])
            
            return NSString(string: String(found)).doubleValue
        }
        
        let string = (json["location"] as? [String])?.first ?? ""
        self.longitude = location(forKey: "longitude", in: string)
        self.latitude = location(forKey: "latitude", in: string)
        
    }
    
    
    
    final class func removeMissingEnities(in json: [JSON], from context: NSManagedObjectContext, matching predicate: NSPredicate?) {
        
        let request: NSFetchRequest<Place> = Place.fetchRequest()
        
        var predicates: [NSPredicate?] = [ predicate ]
        if json.count > 0 {
            let ids: [String] = json.map{ $0["id"] as? String }.compactMap{ $0 }
            if ids.count > 0 {
                predicates.append(NSPredicate(format: "NOT (%K IN %@)", #keyPath(Place.id), ids))
            }
        }
        request.predicate = NSCompoundPredicate.init(andPredicateWithSubpredicates: predicates.compactMap{ $0 })
        
        let results = try? context.fetch(request)
        for obj in results ?? [] {
            context.delete(obj)
        }
        
    }
}
