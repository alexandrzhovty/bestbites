//
//  Endpoint.swift
//  BestBites
//
//  Created by Aleksandr Zhovtyi on 5/3/18.
//  Copyright © 2018 Aleksandr Zhovtyi. All rights reserved.
//

import Foundation

enum Endpoint {
    case locations(pageNumber: Int)
    case register(token: String)
}

extension Endpoint {
    var request: URLRequest {
        let path: String
        let queryItems: [URLQueryItem]
        switch self {
        case .locations(let page):
            path = Settings.apiServerURL.path + "listing"
            queryItems = [
                URLQueryItem(name: "per_page", value: "100"),
                URLQueryItem(name: "page", value: String(format: "%d", page))
            ]
        
        case .register(let token):
            path = "/push/savetoken"
            queryItems = [
                URLQueryItem(name: "auth_key", value: "EerM0RZQ8BHjsZd"),
                URLQueryItem(name: "device_token", value: token),
                URLQueryItem(name: "device_type", value: "ios"),
            ]
            
            
        }
        
        var urlCompoents = URLComponents()
        urlCompoents.scheme = Settings.apiServerURL.scheme
        urlCompoents.host = Settings.apiServerURL.host
        urlCompoents.path = path
        urlCompoents.queryItems = queryItems
        
        return URLRequest(url: urlCompoents.url!)
    }
}
