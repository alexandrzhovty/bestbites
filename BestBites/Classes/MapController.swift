//
//  MapController.swift
//  BestBites
//
//  Created by Aleksandr Zhovtyi on 5/14/18.
//  Copyright © 2018 Aleksandr Zhovtyi. All rights reserved.
//

import UIKit
import MapKit
import CoreData
import UserNotifications
import SafariServices

class MapController: UIViewController {
    @IBOutlet weak var mapView: MKMapView!
    var viewContext: NSManagedObjectContext = Database.default.viewContext
    var geotifications: [Geotification] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        mapView.showsUserLocation = CLLocationManager.locationServicesEnabled()
        
        loadAllGeotifications()
        
        
        let center = UNUserNotificationCenter.current()
        center.getPendingNotificationRequests { array in
            print("Pending request count: ", array.count)
        }
        
    }
    
    @IBAction func didTapTest(_ sender: Any) {
        let content = UNMutableNotificationContent()
        content.title = "Meeting Reminder"
        content.subtitle = "messageSubtitle"
        content.body = "Don't forget to bring coffee."
        content.userInfo = ["link": "http://www.liga.net"]
        
        //        let url = URL(string: "http://www.liga.net")!
        //        do {
        //            let attach = try UNNotificationAttachment(identifier: "link", url: url, options: [:])
        //            content.attachments = [attach]
        //        }
        //        catch {
        //            print("👍 ", String(describing: type(of: self)),":", #function, " ", error)
        //        }
        content.badge = 1
        
        content.categoryIdentifier = actionCategoryIdentifier
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
        
        let requestIdentifier = "linkNotification"
        let request = UNNotificationRequest(identifier: requestIdentifier, content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request, withCompletionHandler: { (error) in
            // Handle error
        })
    }
    
    @IBAction func didTapGoToMyLocation(_ sender: Any?) {
        mapView.zoomToUserLocation()
    }
    
    @IBAction func didTapRefresh(_ sender: Any?) {
        geotifications.forEach(removeGeotification)
        geotifications.removeAll()
        loadAllGeotifications()
    }
    
    
    @IBAction func didTapAddRegion(_ sender: Any?) {
        // 1. Current location
        let status = CLLocationManager.authorizationStatus()
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            break
        default: return
        }
        
        guard let location = mapView.userLocation.location else { return }
        
        
        // 2. Reverse geocoding
        let geocoder = CLGeocoder()
        
        geocoder.reverseGeocodeLocation(location) {
            [weak self] (placemarks, error) in
            guard error == nil else { return }
            guard let firstPlace = placemarks?.first else { return }
            let address: [String?] = [firstPlace.thoroughfare, firstPlace.subThoroughfare, firstPlace.locality]
            
            // 3. Store
            guard let moc = self?.viewContext else { return }
            moc.perform {
                let place = Place(context: moc)
                place.id = Int64(Date().timeIntervalSince1970)
                place.title = address.compactMap{ $0 }.joined(separator: ", ")
                place.location = location
                place.link = URL(string: "http://liga.net")
                
                if moc.hasChanges { try? moc.save() }
                guard UserDefaults.standard.isTrackingEnabled else { return }
                
                // 4. Start monitoring region
                DispatchQueue.main.async {
                    let locationManager = LocationManager.shared
                    guard let location = self?.mapView.userLocation.location else { return }
                    AppDelegate.shared.startMonitoring(with: locationManager, for: location)
                    self?.didTapRefresh(nil)
                }
                
            }
            
        }
        
        
        
        
    }
    
}

// MARK: - MapView Delegate
extension MapController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let identifier = "myGeotification"
        if annotation is Geotification {
            var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKPinAnnotationView
            if annotationView == nil {
                annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                annotationView?.canShowCallout = true
                let removeButton = UIButton(type: .custom)
                removeButton.frame = CGRect(x: 0, y: 0, width: 23, height: 23)
                removeButton.setImage(UIImage(named: "DeleteGeotification")!, for: .normal)
                annotationView?.leftCalloutAccessoryView = removeButton
            } else {
                annotationView?.annotation = annotation
            }
            return annotationView
        }
        return nil
    }
    
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if overlay is MKCircle {
            let circleRenderer = MKCircleRenderer(overlay: overlay)
            circleRenderer.lineWidth = 1.0
            circleRenderer.strokeColor = .purple
            circleRenderer.fillColor = UIColor.purple.withAlphaComponent(0.4)
            return circleRenderer
        }
        return MKOverlayRenderer(overlay: overlay)
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        // Delete geotification
        let geotification = view.annotation as! Geotification
        removeGeotification(geotification)
        saveAllGeotifications()
    }
    
    // MARK: Loading and saving functions
    func loadAllGeotifications() {
        //        geotifications = []
        //        guard let savedItems = UserDefaults.standard.array(forKey: PreferencesKeys.savedItems) else { return }
        //        for savedItem in savedItems {
        //            guard let geotification = NSKeyedUnarchiver.unarchiveObject(with: savedItem as! Data) as? Geotification else { continue }
        //            add(geotification: geotification)
        //        }
        
        let regions = LocationManager.shared.monitoredRegions
        for region in regions {
            guard let circularRegion = region as? CLCircularRegion else { continue }
            //            let ceotification = Geotification(region: circularRegion)
            add(circularRegion)
        }
        
    }
    
    func saveAllGeotifications() {
        //        var items: [Data] = []
        //        for geotification in geotifications {
        //            let item = NSKeyedArchiver.archivedData(withRootObject: geotification)
        //            items.append(item)
        //        }
        //        UserDefaults.standard.set(items, forKey: PreferencesKeys.savedItems)
    }
    
    // MARK: Functions that update the model/associated views with geotification changes
    func add(_ region: CLCircularRegion) {
        let  geotification = Geotification(region: region)
        geotifications.append(geotification)
        mapView.addAnnotation(geotification)
        addRadiusOverlay(forGeotification: geotification)
        updateGeotificationsCount()
    }
    
    func removeGeotification(_ geotification: Geotification) {
        if let indexInArray = geotifications.index(of: geotification) {
            geotifications.remove(at: indexInArray)
        }
        mapView.removeAnnotation(geotification)
        removeRadiusOverlay(forGeotification: geotification)
        updateGeotificationsCount()
    }
    
    func updateGeotificationsCount() {
        title = "Geotifications (\(geotifications.count))"
    }
    
    // MARK: Map overlay functions
    func addRadiusOverlay(forGeotification geotification: Geotification) {
        mapView?.addOverlay(MKCircle(center: geotification.coordinate, radius: geotification.radius))
    }
    
    func removeRadiusOverlay(forGeotification geotification: Geotification) {
        // Find exactly one overlay which has the same coordinates & radius to remove
        guard let overlays = mapView?.overlays else { return }
        for overlay in overlays {
            guard let circleOverlay = overlay as? MKCircle else { continue }
            let coord = circleOverlay.coordinate
            if coord.latitude == geotification.coordinate.latitude && coord.longitude == geotification.coordinate.longitude && circleOverlay.radius == geotification.radius {
                mapView?.removeOverlay(circleOverlay)
                break
            }
        }
    }
    
}


extension MKMapView {
    func zoomToUserLocation() {
        guard let coordinate = userLocation.location?.coordinate else { return }
        let region = MKCoordinateRegion(center: coordinate, latitudinalMeters: 1000, longitudinalMeters: 1000)
        setRegion(region, animated: true)
    }
}
