# BestBites



## Requirements

### Build

Xcode 9.3, iOS 11.0 SDK

### Runtime

iOS 11.0 or later

### 3d party frameworks

|   |   |   |  |  |
|---|---|---|---|---|
|  ![Pod Version](https://img.shields.io/cocoapods/v/SVProgressHUD.svg?style=flat) | ![Pod Platform](https://img.shields.io/cocoapods/p/SVProgressHUD.svg?style=flat) | ![Pod License](https://img.shields.io/cocoapods/l/SVProgressHUD.svg?style=flat) | ![SVProgressHUD](https://github.com/SVProgressHUD/SVProgressHUD) | [![Carthage compatible](https://img.shields.io/badge/Carthage-compatible-4BC51D.svg?style=flat)](https://github.com/Carthage/Carthage) |
|   |   |   |  |  |
|   |   |   |  |  |

### Description
[Main site](https://bestbitesthailand.com)


### Screenshots
[Main page](http://screenshots.collabstar.com/np/2018-05-01-10-32-55.png)
[Start screen](http://screenshots.collabstar.com/np/2018-05-01-10-27-18.png)
[Terms](http://screenshots.collabstar.com/np/2018-05-01-10-28-03.png)
[Settings](http://screenshots.collabstar.com/np/2018-05-01-10-28-23.png)
[-](http://screenshots.collabstar.com/np/2018-05-01-10-28-40.png)

Шрифты, тексты, имейджи, json с плейсами (пока нужно замокать, бекенд еще готовит апи)

### Fonts, texts, images
[Drop Box](https://www.dropbox.com/s/bvggdmba4185oyy/Thailand%20App.zip?dl=0)

### API endpoints
[All locations](https://bestbitesthailand.com/wp-json/wp/v2/listing?per_page=100&page=1)

## Backend
[Administrative part](https://bestbitesthailand.com/wp-admin/)
lindenvalley
FGAgWlWM0v9D3%T73zBU6Q5p


## Changes from Previous Version
This is the first version


0x1f3d8a


Would love to send you Push Notifications from our BestBites Thailand Members with Vouchers and Invitatios.
Yes, I want in!       No thanks


Would you like use you location for to show you nearby BestBites Thailand Members
Yes, I agree        No thanks



Copyright (C) 2016 Famer. All rights reservexd.


